# API Testing

API Documentation: https://restful-booker.herokuapp.com/apidoc/index.html

**Pre-requisite**
----
- Ruby installed.
- Cucumber installed `gem install cucumber`
- Rest-client installed `gem install rest-client`

**How to run**
----
From root folder run:
```
cucumber
```

**Result**
----
![Result](result.png)
