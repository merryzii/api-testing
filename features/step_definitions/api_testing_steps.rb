url = method =  request = response = bookingId = scenarioName = header = nil
urlBooking = 'https://restful-booker.herokuapp.com/booking/'
urlToken = 'https://restful-booker.herokuapp.com/auth'
bookingRequest = {
    'firstname' => 'Mery',
    'lastname' => 'S',
    'totalprice' => 111,
    'depositpaid' => true,
    'bookingdates' => {
      'checkin' => '2020-05-10',
      'checkout' => '2020-05-11'
    },
    'additionalneeds' => 'Breakfast'
}
tokenRequest = {
    'username' => 'admin',
    'password' => 'password123'
  }

Before do |scenario|
    scenarioName = scenario.name
end

When(/^I want to create a booking$/) do
    url = urlBooking
    method = :post
    request = bookingRequest
    header = nil
end

When(/^I want to check booking$/) do
    if bookingId.to_s == ''
        jsonResponse = JSON.parse(response.body)
        bookingId = jsonResponse['bookingid'].to_s
        puts 'Booking ID: ' + bookingId
    end

    url = urlBooking + bookingId
    method = :get
    request = nil
    header = nil
end

When(/^I want to delete booking$/) do
    url = urlBooking + bookingId
    method = :delete
    request = nil
    header = nil
end

Then(/^the response code should be (.*)$/) do |code|
    begin
        response = RestClient::Request.execute(method: method, url: url, payload: request, headers: header)
        assert_equal(code, response.code.to_s)
    rescue RestClient::ExceptionWithResponse => err
        assert_equal(code, err.response.code.to_s)
    end
end

Then(/^the response should be match with booking$/) do
    jsonResponse = JSON.parse(response.body)
    if scenarioName == 'Success booking'
        jsonResponse = jsonResponse['booking']
    end
    assert_equal(bookingRequest['firstname'], jsonResponse['firstname'])
    assert_equal(bookingRequest['lastname'], jsonResponse['lastname'])
    assert_equal(bookingRequest['totalprice'], jsonResponse['totalprice'])
    assert_equal(bookingRequest['depositpaid'], jsonResponse['depositpaid'])
    assert_equal(bookingRequest['bookingdates']['checkin'], jsonResponse['bookingdates']['checkin'])
    assert_equal(bookingRequest['bookingdates']['checkout'], jsonResponse['bookingdates']['checkout'])
    assert_equal(bookingRequest['additionalneeds'], jsonResponse['additionalneeds'])
end

Then(/^I should get a token$/) do
    url = urlToken
    method = :post
    request = tokenRequest
    header = nil
end

Then(/^I delete booking with token$/) do
    jsonResponse = JSON.parse(response.body)
    
    url = urlBooking + bookingId
    method = :delete
    request = nil
    header = {
        'Cookie' => 'token=' + jsonResponse['token'] 
    }
end
