Feature: Booking

    Scenario: Success booking
        When I want to create a booking
        Then the response code should be 200
        And the response should be match with booking

    Scenario: Check booking
        When I want to check booking
        Then the response code should be 200
        And the response should be match with booking

    Scenario: Delete booking without token
        When I want to delete booking
        Then the response code should be 403
    
    Scenario: Delete booking with token
        When I want to delete booking
        Then I should get a token
        And the response code should be 200
        Then I delete booking with token
        And the response code should be 201

    Scenario: Check booking deleted
        When I want to check booking
        Then the response code should be 404
